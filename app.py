from flask import Flask, render_template, request, url_for
from imageai.Detection import ObjectDetection
import os

UPLOAD_FOLDER = 'static'
INPUT_FILENAME = 'file.jpg'
OUTPUT_FILENAME = 'file_new.jpg'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['INPUT_FILENAME'] = INPUT_FILENAME
app.config['OUTPUT_FILENAME'] = OUTPUT_FILENAME


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == "POST":
        file = request.files['file']
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], app.config['INPUT_FILENAME']))
        description = detection()
        print(os.path.join(app.config['UPLOAD_FOLDER'], app.config['OUTPUT_FILENAME']))
        return render_template("detection.html", description=description,
                               file_new=os.path.join(app.config['UPLOAD_FOLDER'], app.config['OUTPUT_FILENAME']))
    else:
        return render_template("index.html")


@app.route('/info', methods=['GET'])
def info():
    return render_template("info.html")


def detection():
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

    detector = ObjectDetection()
    detector.setModelTypeAsTinyYOLOv3()
    detector.setModelPath("yolo-tiny.h5")
    detector.loadModel()
    detections = detector.detectObjectsFromImage(
        input_image=os.path.join(app.config['UPLOAD_FOLDER'], app.config['INPUT_FILENAME']),
        output_image_path=os.path.join(app.config['UPLOAD_FOLDER'], app.config['OUTPUT_FILENAME']),
        minimum_percentage_probability=30)
    for eachObject in detections:
        print(eachObject["name"], " : ", eachObject["percentage_probability"], " : ", eachObject["box_points"])
        print("--------------------------------")
    return getDescription(detections)


def getDescription(detections):
    persons = 0
    cars = 0
    bicycles = 0
    dogs = 0
    cats = 0

    for eachObject in detections:
        if eachObject["name"] == "person":
            persons += 1
        if eachObject["name"] == "car":
            cars += 1
        if eachObject["name"] == "dog":
            dogs += 1
        if eachObject["name"] == "cat":
            cats += 1
        if eachObject["name"] == "bicycle":
            bicycles += 1

    description = "Found: \n {} \t persons \n {} \t cars \n {} \t bicycles \n {} \t dogs \n {} \t cats" \
        .format(persons, cars, bicycles, dogs, cats)
    return description


if __name__ == "__main__":
    app.run(debug=True)
