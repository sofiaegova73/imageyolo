
from imageai.Detection import ObjectDetection
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

detector = ObjectDetection()
detector.setModelTypeAsTinyYOLOv3()
detector.setModelPath("yolo-tiny.h5")
detector.loadModel()
detections = detector.detectObjectsFromImage(input_image="road.jpeg",
                                             output_image_path="imagenew.jpg",
                                             minimum_percentage_probability=30)

for eachObject in detections:
    print(eachObject["name"] , " : ", eachObject["percentage_probability"], " : ", eachObject["box_points"] )
    print("--------------------------------")

