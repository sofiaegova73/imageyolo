# ImageYolo


Python      3.7


Flask       2.0.2
h5py        2.10
imageai     2.1.6
keras       2.7
matprolib   3.3.2
numpy       1.19.3
scipy       1.4.1
tensorflow  2.7

